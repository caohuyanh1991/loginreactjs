import { Redirect } from 'react-router-dom';

export default function HomePage() {
  return (
    <Redirect to='/login' />
  )
}