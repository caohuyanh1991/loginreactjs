import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import HomePage from './components/HomePage';
import Login from "./components/Login";
import Register from "./components/Register";

function App() {
  return (
    <Router>
      <Route path="/" component={HomePage} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
  </Router>
  )
}
export default App;