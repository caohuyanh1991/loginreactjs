require('dotenv').config()
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const router = express.Router()
app.use(express.json())

mongoose.connect(process.env.MONGODB_URL || "", { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log("mongoose connected")
})


const useRouter = require('./src/routers/user');
app.use('/api', useRouter)


app.listen(process.env.PORT || 3000, () => {
  console.log('server is listening on port ' + process.env.PORT || 3000)
});