const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const verifyToken = require('../middleware/auth');

router.get('/', async (req, res, next) => {
  const user = await User.find()
  res.json({
    success: true,
    data: user
  })
})

router.post('/login', async (req, res, next) => {
  const { username, password } = req.body;
  if (!username || !password) {
    return res.status(400).json({ success: false, message: 'Tên người dùng hoặc mật khẩu không được để trống' })
  }
  try {
    const user = await User.findOne({ username })
    if (!user)
      return res.status(400).json({
        success: false,
        message: 'Tên đăng nhập hoặc mật khẩu không chính xác'
      })
    const passwordValid = await argon2.verify(user.password, password)
    if (!passwordValid)
      return res.status(400).json({
        success: false,
        message: 'Tên đăng nhập hoặc mật khẩu không chính xác'
      })
    const accessToken = jwt.sign({ userID: user._id }, process.env.ACCESS_TOKEN_SECRET)
    res.json({
      success: true,
      message: 'Đăng nhập thành công',
      user,
      accessToken
    })
  }
  catch (err) {
    res.status(500).json({
      success: false,
      message: err.message
    })
  }
})

router.post('/logout', (req, res) => {
  try {
    res.status(200).json({
      success: true,
      message: ' Logout thành công'
    })
  }
  catch (err) {
    res.status(500).json({
      success: false,
      message: err.message
    })
  }
})

router.post('/register', async (req, res) => {
  const { username, password, email, role } = req.body;
  if (!username || !email || !password) {
    return res.status(400).json({ success: false, message: 'Tên người dùng, email hoặc mật khẩu không được để trống' })
  }
  try {

    const user = await User.findOne({ username })
    if (user) {
      return res.status(400).json({ success: false, message: 'Tên người dùng đã tồn tại' })
    }
    const hashedPassword = await argon2.hash(password);
    const newUser = new User({
      username,
      email,
      password: hashedPassword,
      role
    })
    await newUser.save()
    const accessToken = jwt.sign({ userId: newUser._id }, process.env.ACCESS_TOKEN_SECRET)
    res.json({ success: true, message: 'Tạo người dùng thành công', accessToken })
  }
  catch (err) {
    res.status(500).json({
      success: false,
      message: err.message
    })
  }
})
module.exports = router;